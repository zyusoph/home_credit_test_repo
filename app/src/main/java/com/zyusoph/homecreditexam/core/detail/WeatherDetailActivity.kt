package com.zyusoph.homecreditexam.core.detail

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import com.zyusoph.homecreditexam.R
import com.zyusoph.homecreditexam.core.base.BaseActivity
import com.zyusoph.homecreditexam.core.model.WeatherModel
import kotlinx.android.synthetic.main.activity_weather_detail.*

/**
 * Created by z.y on 11/24/2018.
 */
class WeatherDetailActivity: BaseActivity() {

    private lateinit var mWeatherDetailViewModel: WeatherDetailViewModel

    companion object {

        fun newInstance(context: Context, selectedWeatherModel: WeatherModel): Intent {

            val gsonified = Gson().toJson(selectedWeatherModel)

            val intent = Intent(context, WeatherDetailActivity::class.java)
            intent.putExtra(WeatherDetailViewModel.KEY_WEATHER_MODEL_GSON_INTENT, gsonified)

            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_detail)

        mWeatherDetailViewModel = WeatherDetailViewModel()
        observeInit()
    }

    override fun observeInit() {

        mWeatherDetailViewModel.liveInitEvent().observe(this, Observer {

            val gsonified = intent.getStringExtra(WeatherDetailViewModel.KEY_WEATHER_MODEL_GSON_INTENT)
            val weatherModel = Gson().fromJson<WeatherModel>(gsonified, WeatherModel::class.java)

            mDetailPlace.text = weatherModel.place
            mDetailCondition.text = weatherModel.condition
            mDetailDescription.text = weatherModel.description
            mDetailTemperature.text = "${weatherModel.temperature}°C"
            mDetailPressure.text = weatherModel.pressure
            mDetailHumidity.text = weatherModel.humidity
            mDetailTempMin.text = weatherModel.minTemp
            mDetailTempMax.text = weatherModel.maxTemp
            mDetailVisibility.text = weatherModel.visibility
            mDetailWindSpeed.text = weatherModel.windSpeed
            mDetailWindDegree.text = weatherModel.windDegree

            Picasso.with(this).load("http://openweathermap.org/img/w/${weatherModel.iconSrc}.png")
                    .into(mDetailIcon)
        })
    }
}