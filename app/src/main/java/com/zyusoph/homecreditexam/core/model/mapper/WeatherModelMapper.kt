package com.zyusoph.homecreditexam.core.model.mapper

import com.zyusoph.homecreditexam.core.model.WeatherModel
import com.zyusoph.homecreditexam.data.network.response.OpenWeatherCurrentConditionResponse
import com.zyusoph.homecreditexam.data.repository.entity.LastWeatherEntity
import com.zyusoph.homecreditexam.util.fromDegreeToCardinal

/**
 * Created by z.y on 11/23/2018.
 */
class WeatherModelMapper {

    companion object {

        fun transform(response: OpenWeatherCurrentConditionResponse, locationName: String): WeatherModel {

            val weatherModel = WeatherModel(
                    locationName,
                    "${response.main.temperature}",
                    response.weather[0].icon,
                    response.weather[0].main,
                    response.weather[0].description,
                    "${response.main.pressure}",
                    "${response.main.humidity}",
                    "${response.main.temperatureMin}°C",
                    "${response.main.temperatureMax}°C",
                    "${response.visibility}",
                    "${response.wind.speed} kph",
                    fromDegreeToCardinal(response.wind.deg)
                    )

            return weatherModel
        }

        fun transform(lastWeatherEntites: List<LastWeatherEntity>): List<WeatherModel> {

            val weatherModels = ArrayList<WeatherModel>()

            lastWeatherEntites.forEach {
                entity: LastWeatherEntity->

                val weatherModel = WeatherModel(
                        entity.place,
                        entity.temperature,
                        entity.iconSrc,
                        entity.condition,
                        entity.description,
                        entity.pressure,
                        entity.humidity,
                        entity.minTemp,
                        entity.maxTemp,
                        entity.visibility,
                        entity.windSpeed,
                        entity.windDegree
                )

                weatherModels.add(weatherModel)
            }

            return weatherModels
        }
    }
}