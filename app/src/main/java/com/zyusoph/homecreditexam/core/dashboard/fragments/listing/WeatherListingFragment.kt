package com.zyusoph.homecreditexam.core.dashboard.fragments.listing

import android.Manifest
import android.arch.lifecycle.Observer
import android.content.Context.LOCATION_SERVICE
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.zyusoph.homecreditexam.R
import com.zyusoph.homecreditexam.constants.RequestResponseFlags
import com.zyusoph.homecreditexam.core.base.BaseFragment
import com.zyusoph.homecreditexam.core.dashboard.DashboardViewModel
import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.components.WeatherItemAdapter
import com.zyusoph.homecreditexam.core.model.WeatherModel
import com.zyusoph.homecreditexam.data.network.service.OpenWeatherService
import com.zyusoph.homecreditexam.data.repository.WeatherRepository
import com.zyusoph.homecreditexam.data.repository.database.AppDatabase
import com.zyusoph.homecreditexam.exception.NoWeatherDataException
import com.zyusoph.homecreditexam.util.hasInternetConnectivity
import com.zyusoph.homecreditexam.util.isGpsEnabled
import kotlinx.android.synthetic.main.fragment_weather_listing.*
import retrofit2.HttpException
import java.lang.ref.WeakReference
import java.net.UnknownHostException

/**
 * Created by z.y on 11/23/2018.
 */
class WeatherListingFragment: BaseFragment(), GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private var mGoogleApiClient: GoogleApiClient?  = null
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private  var mLocationRequest: LocationRequest? = null
    private lateinit var mLocationSettingsRequestBuilder:  LocationSettingsRequest.Builder
    private var mLocationUpdateReceived = false
    private var mIsRunning = false
    private var mBaseLocationCallback = BaseLocationCallback()
    private lateinit var mWeatherListingViewModel: WeatherListingViewModel
    private lateinit var mInitCallback: InitCallback
    private lateinit var mDashboardViewModel: DashboardViewModel
    private lateinit var mLocationManager: LocationManager
    private val permissionList = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)

    companion object {
        fun newInstance(): WeatherListingFragment = WeatherListingFragment()
    }

    override fun onCreateView(inflater : LayoutInflater, viewGroup : ViewGroup?, savedInstanceState : Bundle?) : View {
        return inflater.inflate(R.layout.fragment_weather_listing, viewGroup, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        if (mLocationRequest == null) {

            mLocationRequest = LocationRequest.create()
            mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            mLocationRequest!!.interval = 5000
            mLocationRequest!!.fastestInterval = 3000
        }

        if (mGoogleApiClient == null) {

            mGoogleApiClient = GoogleApiClient.Builder(this.activity!!)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view!!, savedInstanceState)

        mWeatherListingViewModel = WeatherListingViewModel(
                WeatherRepository(
                        OpenWeatherService(), AppDatabase.getAppDatabase(this.activity!!)!!
                ))
        observeInit()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == RequestResponseFlags.PERMISSION_REQUEST) {

            if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this.activity, "Location access denied", Toast.LENGTH_LONG).show()
            } else {

                if (isGpsEnabled(WeakReference(this.activity!!.applicationContext))) {
                    requestLastLocation()
                } else {
                    requestLocationSettings()
                }
            }
        }
    }

    override fun onStop() {

        mGoogleApiClient!!.disconnect()
        super.onStop()
    }

    override fun onResume() {

        mLocationUpdateReceived = false
        mIsRunning = false

        if (!mGoogleApiClient!!.isConnected) {
            mGoogleApiClient?.connect()
        }

        super.onResume()
    }

    override fun onDestroy() {

        mGoogleApiClient!!.disconnect()
        super.onDestroy()
    }

    override fun onConnected(p0: Bundle?) {
        Log.e("GoogleClient", "Connected")
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.e("BaseLocationActivity", "Connection Suspended")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.e("BaseLocationActivity", "Connection Failed, ${p0.errorMessage} , ${p0.errorCode}")
    }

    override fun observeInit() {

        mWeatherListingViewModel.liveInitEvent().observe(this, Observer {

            observeProgress()
            observeError()
            observeLiveWeatherDataEvent()

            mInitCallback.onInit(mWeatherListingViewModel)

            mLocationManager = activity!!.getSystemService(LOCATION_SERVICE) as LocationManager

            mDashboardGpsIcon.setOnClickListener({

                if (hasInternetConnectivity(WeakReference(this.activity!!))) {

                    if (unPermitted()) {
                        requestPermissions(permissionList, RequestResponseFlags.PERMISSION_REQUEST)
                    } else {

                        if (!isGpsEnabled(WeakReference(this.activity!!.applicationContext))) {
                            requestLocationSettings()
                        } else {

                            mWeatherListingViewModel.onLocationRequest()
                            requestLastLocation()
                        }
                    }
                }
            })
        })
    }

    override fun observeError() {

        mWeatherListingViewModel.liveErrorEvent().observe(this, Observer {
            error: Throwable? ->
            error!!

            if (error is UnknownHostException || error is HttpException) {

                mProgressWeatherListing.visibility = View.GONE
                mDashboardErrorMessage.text = "There seems to be a problem with your connectivity."
                mDashboardErrorMessage.visibility = View.VISIBLE

            } else if (error is RuntimeException) {

                mProgressWeatherListing.visibility = View.GONE
                mDashboardErrorMessage.text = "There seems to be a problem with your connectivity. And" +
                        " there are no available last weather to be displayed."

                mDashboardErrorMessage.visibility = View.VISIBLE
            }
        })
    }

    private fun unPermitted(): Boolean {

        val permissionList = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
        val coarsePermission = ContextCompat.checkSelfPermission(this.activity!!.applicationContext, permissionList[0])
        val finePermission = ContextCompat.checkSelfPermission(this.activity!!.applicationContext, permissionList[1])

        return  coarsePermission != PackageManager.PERMISSION_GRANTED && finePermission != PackageManager.PERMISSION_GRANTED
    }

    private fun observeLiveWeatherDataEvent() {

        mWeatherListingViewModel.liveWeatherDataEvent().observe(this, Observer {
            weatherModels: List<WeatherModel>? ->
            weatherModels!!

            // This could've been better if I can have enough time to implement DiffUtil for
            // updating the adapter's backing data
            with (mRecyclerWeatherListing) {

                layoutManager = LinearLayoutManager(this.context)
                adapter = WeatherItemAdapter(weatherModels, mDashboardViewModel)
            }
        })
    }

    private fun observeProgress() {

        mWeatherListingViewModel.liveProgressEvent().observe(this, Observer {
            showProgress: Boolean? ->
            showProgress!!

            mDashboardErrorMessage.visibility = View.GONE

            if (showProgress) {
                mProgressWeatherListing.visibility = View.VISIBLE
                mRecyclerWeatherListing.visibility = View.GONE
            } else {
                mRecyclerWeatherListing.visibility = View.VISIBLE
                mProgressWeatherListing.visibility = View.GONE

                if (!hasInternetConnectivity(WeakReference(this.activity!!.applicationContext))) {
                    mDashboardLastKnownWeatherMessage.visibility = View.VISIBLE
                } else {
                    mDashboardLastKnownWeatherMessage.visibility = View.GONE
                }
            }
        })

        mWeatherListingViewModel.liveLocationProgressEvent().observe(this, Observer {
            showProgress: Boolean? ->
            showProgress!!

            if (showProgress) {

                mDashboardLocationInProgress.visibility = View.VISIBLE
                mDashboardGpsIcon.visibility = View.INVISIBLE
            } else {

                mDashboardLocationInProgress.visibility = View.GONE
                mDashboardGpsIcon.visibility = View.VISIBLE
            }
        })
    }

    fun setInitCallback(initCallback: InitCallback) {
        mInitCallback = initCallback
    }

    fun setParentViewModel(dashboardViewModel: DashboardViewModel) {
        mDashboardViewModel = dashboardViewModel
    }

    fun requestLastLocation() {

        Log.e("BaseLocationActivity", "${mGoogleApiClient!!.isConnected}")

        if (!mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.connect()
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)

        mFusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->

            if (mIsRunning) {

                if (mLocationUpdateReceived) {

                    mWeatherListingViewModel.onLocationReceived()
                    mDashboardCurrentLocationLatLng.text = "${location?.latitude} / ${location?.longitude}"
                }
                else {

                    mWeatherListingViewModel.onLocationRequest()
                    mIsRunning = true

                    mBaseLocationCallback = BaseLocationCallback()
                    mFusedLocationClient.requestLocationUpdates(mLocationRequest, mBaseLocationCallback, null)
                }
            }
            else {

                mWeatherListingViewModel.onLocationRequest()
                mIsRunning = true

                mBaseLocationCallback = BaseLocationCallback()
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mBaseLocationCallback, null)
            }
        }
    }

    fun requestLocationSettings() {

        mLocationSettingsRequestBuilder = LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest!!)

        mLocationSettingsRequestBuilder.setAlwaysShow(true)

        val result = LocationServices.getSettingsClient(this.activity!!).checkLocationSettings(mLocationSettingsRequestBuilder.build())

        result.addOnCompleteListener({
            task: Task<LocationSettingsResponse> ->

            try {
                task.getResult(ApiException::class.java)
            }
            catch (e: ApiException) {

                when(e.statusCode) {

                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {

                        try {

                            val resolvable = e as ResolvableApiException
                            resolvable.startResolutionForResult(this@WeatherListingFragment.activity, RequestResponseFlags.LOCATION_SETTINGS_REQUEST)
                        }
                        catch (e: IntentSender.SendIntentException) {
                            Log.e("SettingsIntentTask", "Location Settings, ${e.message}")
                        }
                        catch (e: ClassCastException) {
                            Log.e("SettingsIntentTask", "Class Cast, ${e.message}")
                        }
                    }
                }
            }
        })
    }

    fun gpsSet() {

        if (unPermitted()) {
            requestPermissions(permissionList, RequestResponseFlags.PERMISSION_REQUEST)
        } else {
            mWeatherListingViewModel.onLocationRequest()
            requestLastLocation()
        }
    }

    inner class BaseLocationCallback : LocationCallback() {

        override fun onLocationResult(var1: LocationResult) {

            Log.e("BaseLocationActivity", "On Location Result")

            if (!mLocationUpdateReceived) {

                mLocationUpdateReceived = true

                mWeatherListingViewModel.onLocationReceived()
                val location = var1!!.lastLocation
                mDashboardCurrentLocationLatLng.text = "${location?.latitude} / ${location?.longitude}"
            }
        }

        override fun onLocationAvailability(var1: LocationAvailability) {
            Log.e("BaseLocationActivity", "On Location Availability")
        }
    }

    interface InitCallback {
        fun onInit(viewModel: WeatherListingViewModel)
    }
}