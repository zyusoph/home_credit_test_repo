package com.zyusoph.homecreditexam.core.dashboard.fragments.refresh

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zyusoph.homecreditexam.R
import com.zyusoph.homecreditexam.core.base.BaseFragment
import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.WeatherListingViewModel
import kotlinx.android.synthetic.main.fragment_refresh_listing.*

/**
 * Created by z.y on 11/23/2018.
 */
class RefreshListingFragment: BaseFragment() {

    private lateinit var mRefreshListingViewModel: RefreshListingViewModel
    private lateinit var mWeatherListingViewModel: WeatherListingViewModel

    companion object {
        fun newInstance(): RefreshListingFragment = RefreshListingFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, viewGroup: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_refresh_listing, viewGroup, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view!!, savedInstanceState)

        mRefreshListingViewModel = RefreshListingViewModel()
        observeInit()
    }

    override fun observeInit() {

        mRefreshListingViewModel.liveInitEvent().observe(this, Observer {

            mButtonRefreshListing.setOnClickListener({
                mWeatherListingViewModel.refresh()
            })
        })
    }

    override fun observeError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

     fun setListingViewModel(listingViewModel: WeatherListingViewModel) {
        mWeatherListingViewModel = listingViewModel
    }
}