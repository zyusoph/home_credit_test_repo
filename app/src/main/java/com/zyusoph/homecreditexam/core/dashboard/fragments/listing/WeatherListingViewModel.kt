package com.zyusoph.homecreditexam.core.dashboard.fragments.listing

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.zyusoph.homecreditexam.core.base.BaseViewModel
import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.components.targetLocations
import com.zyusoph.homecreditexam.core.model.WeatherModel
import com.zyusoph.homecreditexam.core.model.mapper.WeatherModelMapper
import com.zyusoph.homecreditexam.data.repository.WeatherRepository
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.SocketException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

/**
 * Created by z.y on 11/23/2018.
 */
class WeatherListingViewModel(weatherRepository: WeatherRepository): BaseViewModel() {

    private val liveWeatherDataWatcher = MutableLiveData<List<WeatherModel>>()
    private val weatherRepository = weatherRepository
    private val liveLocationProgressWatcher = MutableLiveData<Boolean>()

    // London 51.5074, 0.1278
    // Prauge 50.0755, 14.4378
    // San Fransisco 37.7749, 122.4194

    init {

        liveInitWatcher.postValue(null)

        fetch()
                .subscribeOn(Schedulers.io())
                .subscribe(object: SingleObserver<List<WeatherModel>> {
                    override fun onSuccess(t: List<WeatherModel>) {
                        liveWeatherDataWatcher.postValue(t)
                        liveProgressWatcher.postValue(false)
                    }

                    override fun onSubscribe(d: Disposable) {
                        liveProgressWatcher.postValue(true)
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        liveErrorWatcher.postValue(e)
                    }
                })
    }

    fun liveWeatherDataEvent(): MutableLiveData<List<WeatherModel>> = liveWeatherDataWatcher
    fun liveLocationProgressEvent(): MutableLiveData<Boolean> = liveLocationProgressWatcher

    fun onLocationRequest() {
        liveLocationProgressWatcher.postValue(true)
    }

    fun onLocationReceived() {
        liveLocationProgressWatcher.postValue(false)
    }

    fun refresh() {

        fetch().subscribeOn(Schedulers.io())
                .subscribe(object: SingleObserver<List<WeatherModel>> {
                    override fun onSuccess(t: List<WeatherModel>) {
                        liveWeatherDataWatcher.postValue(t)
                        liveProgressWatcher.postValue(false)
                    }

                    override fun onSubscribe(d: Disposable) {
                        liveProgressWatcher.postValue(true)
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        liveErrorWatcher.postValue(e)
                    }
                })
    }

    private fun fetch(): Single<List<WeatherModel>> {

        // I did not manage to do this quickly, I had a hard time so I took a peek
        // on my previous project, this aint easy.
        return weatherRepository.fetchCurrentCondition(targetLocations)

    }

    data class TargetLocation(
         val latitude: Double,
         val longitude: Double,
         val place: String
    )
}