package com.zyusoph.homecreditexam.core.dashboard

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.util.Log
import com.google.gson.Gson
import com.zyusoph.homecreditexam.R
import com.zyusoph.homecreditexam.constants.RequestResponseFlags
import com.zyusoph.homecreditexam.constants.RequestResponseFlags.Companion.LOCATION_SETTINGS_REQUEST
import com.zyusoph.homecreditexam.core.base.BaseActivity
import com.zyusoph.homecreditexam.core.base.BaseApplication
import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.WeatherListingFragment
import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.WeatherListingViewModel
import com.zyusoph.homecreditexam.core.dashboard.fragments.refresh.RefreshListingFragment
import com.zyusoph.homecreditexam.core.detail.WeatherDetailActivity
import com.zyusoph.homecreditexam.core.model.WeatherModel
import com.zyusoph.homecreditexam.util.isGpsEnabled
import java.lang.ref.WeakReference

/**
 * Created by z.y on 11/23/2018.
 */
class DashboardActivity: BaseActivity() {

    private lateinit var mDashboardViewModel: DashboardViewModel
    private lateinit var mWeatherListingFragment: WeatherListingFragment
    private lateinit var mRefreshListingFragment: RefreshListingFragment

    companion object {

        fun newInstance(context: Context): Intent {

            val intent = Intent(context, DashboardActivity::class.java)

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        mDashboardViewModel = DashboardViewModel()
        observeInit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RequestResponseFlags.LOCATION_SETTINGS_REQUEST) {

            if (isGpsEnabled(WeakReference(this.applicationContext))) {
                mWeatherListingFragment.gpsSet()
            }
        }
    }

    override fun onBackPressed() {}

    override fun observeInit() {

        mDashboardViewModel.liveInitEvent().observe(this, Observer {

            observeWeatherItemSelection()

            // this is one of the things I answered with my interview, instead
            // of creating a functions/methods for what ever intent, I'd rather
            // choose to slightly bloat or compound a method some how
            // to reduce the method count

            mWeatherListingFragment = WeatherListingFragment.newInstance()
            mRefreshListingFragment = RefreshListingFragment.newInstance()


            // I was trying to implement my old/comfortable ways of communicating fragments
            // by invoking fragment's parent activity (e.g fragment.activity as ParentActivity))
            // and doing things from there, instead I passed the actual fragment's view model
            // to this fragment, hopefully this kind of approach would not inhibit possible
            // leakage on the long run.
            mWeatherListingFragment.setInitCallback(object: WeatherListingFragment.InitCallback{
                override fun onInit(viewModel: WeatherListingViewModel) {
                    mRefreshListingFragment.setListingViewModel(viewModel)
                }
            })

            mWeatherListingFragment.setParentViewModel(mDashboardViewModel)

            // listing fragment
            var fragmentTransaction1 = supportFragmentManager.beginTransaction()

            fragmentTransaction1.add(R.id.mDashboardContainerListing, mWeatherListingFragment, WeatherListingFragment::class.java.name)
            fragmentTransaction1.addToBackStack(WeatherListingFragment::class.java.simpleName)
            fragmentTransaction1.commit()

            // refresh fragment
            var fragmentTransaction2 = supportFragmentManager.beginTransaction()

            fragmentTransaction2.add(R.id.mDashboardContainerRefresh, mRefreshListingFragment, RefreshListingFragment::class.java.name)
            fragmentTransaction2.addToBackStack(RefreshListingFragment::class.java.simpleName)
            fragmentTransaction2.commit()

            supportFragmentManager.executePendingTransactions()

        })
    }

    private fun observeWeatherItemSelection() {

        mDashboardViewModel.liveWeatherItemSelectionEvent().observe(this, Observer {
            weatherModel: WeatherModel? ->
            weatherModel!!
            startActivity(WeatherDetailActivity.newInstance(this, weatherModel))
        })
    }
}