package com.zyusoph.homecreditexam.core.base

import android.support.v4.app.Fragment

/**
 * Created by z.y on 11/23/2018.
 */
abstract class BaseFragment: Fragment() {
    abstract fun observeInit()
    abstract fun observeError()

    override fun onDestroyView() {
        super.onDestroyView()

        val app = this.context as BaseApplication
        app.referenceWatcher().watch(this)
    }
}
