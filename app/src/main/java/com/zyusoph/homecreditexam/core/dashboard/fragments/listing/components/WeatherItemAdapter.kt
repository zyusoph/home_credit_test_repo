package com.zyusoph.homecreditexam.core.dashboard.fragments.listing.components

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.zyusoph.homecreditexam.R
import com.zyusoph.homecreditexam.core.dashboard.DashboardViewModel
import com.zyusoph.homecreditexam.core.model.WeatherModel

/**
 * Created by z.y on 11/23/2018.
 */
class WeatherItemAdapter(
        weatherModels: List<WeatherModel>,
        dashboardViewModel: DashboardViewModel
):  RecyclerView.Adapter<WeatherItemAdapter.WeatherViewHolder>() {

    private val mDashboardViewModel = dashboardViewModel
    private val mWeatherModels = weatherModels
    private lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {

        mContext = parent.context

        // I always name collection item layouts/designs with prefix "item"
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_weather, parent, false)
        return WeatherViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mWeatherModels.size
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {

        val weatherModel = mWeatherModels[position]

        holder.mItemWeatherPlace.text = weatherModel.place
        holder.mItemWeatherTemperature.text = "${weatherModel.temperature}°C"
        holder.mItemWeatherConditionInfo.text = weatherModel.condition

        Picasso.with(mContext).load("http://openweathermap.org/img/w/${weatherModel.iconSrc}.png")
                .into(holder.mItemWeatherConditionIcon)

        holder.itemView.setOnClickListener({
            mDashboardViewModel.onWeatherItemSelected(weatherModel)
        })
    }

    class WeatherViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val mItemWeatherPlace = itemView.findViewById<TextView>(R.id.mItemWeatherPlace)!!
        val mItemWeatherTemperature = itemView.findViewById<TextView>(R.id.mItemWeatherTemperature)!!
        val mItemWeatherConditionIcon = itemView.findViewById<ImageView>(R.id.mItemWeatherConditionIcon)!!
        val mItemWeatherConditionInfo = itemView.findViewById<TextView>(R.id.mItemWeatherConditionInfo)!!
    }
}