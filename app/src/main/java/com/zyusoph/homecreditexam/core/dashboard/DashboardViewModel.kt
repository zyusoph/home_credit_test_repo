package com.zyusoph.homecreditexam.core.dashboard

import android.arch.lifecycle.MutableLiveData
import com.zyusoph.homecreditexam.core.base.BaseViewModel
import com.zyusoph.homecreditexam.core.model.WeatherModel

/**
 * Created by z.y on 11/23/2018.
 */
class DashboardViewModel: BaseViewModel() {

    private val liveWeatherItemSelectionEvent = MutableLiveData<WeatherModel>()

    init {
        liveInitWatcher.postValue(null)
    }

    fun liveWeatherItemSelectionEvent(): MutableLiveData<WeatherModel> = liveWeatherItemSelectionEvent

    fun onWeatherItemSelected(weatherModel: WeatherModel) {
        liveWeatherItemSelectionEvent.postValue(weatherModel)
    }
}