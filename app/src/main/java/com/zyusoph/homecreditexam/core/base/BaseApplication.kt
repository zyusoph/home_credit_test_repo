package com.zyusoph.homecreditexam.core.base

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import com.zyusoph.homecreditexam.BuildConfig
import io.fabric.sdk.android.Fabric

/**
 * Created by z.y on 11/24/2018.
 */
class BaseApplication: Application() {

    private lateinit var mReferenceWatcher: RefWatcher

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())

        // one of the critical things in leak canary is that it sometimes crashes when attempting
        // to dump large heap reports, so its highly advisable not to use this
        // on production stage
        if (BuildConfig.DEBUG) {
            mReferenceWatcher = LeakCanary.install(this)
        }
    }

    fun  referenceWatcher(): RefWatcher {
        return mReferenceWatcher
    }
}