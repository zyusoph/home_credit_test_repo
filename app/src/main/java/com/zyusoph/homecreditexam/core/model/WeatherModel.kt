package com.zyusoph.homecreditexam.core.model

/**
 * Created by z.y on 11/23/2018.
 */
data class WeatherModel(
        val place: String,
        val temperature: String,
        val iconSrc: String,
        val condition: String,
        val description: String,
        val pressure: String,
        val humidity: String,
        val minTemp: String,
        val maxTemp: String,
        val visibility: String,
        val windSpeed: String,
        val windDegree: String
)