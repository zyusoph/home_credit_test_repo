package com.zyusoph.homecreditexam.core.base

import android.arch.lifecycle.MutableLiveData

/**
 * I always prefer to have a default/nullable/generic initialization of
 * fragments or activities (i.e MutableLiveData as type of Any),
 * that way what ever is necessary to initialize on my end
 * can be done without affecting other things need to be done next. I can isolate
 * initialization of things like repositories.
 *
 * Created by z.y on 11/23/2018.
 */
abstract class BaseViewModel {

    protected val liveInitWatcher = MutableLiveData<Any>()
    protected val liveErrorWatcher = MutableLiveData<Throwable>()
    protected val liveProgressWatcher = MutableLiveData<Boolean>()

    // I stick to writing functions/methods with declaring their return type for ease of debugging

    fun liveInitEvent(): MutableLiveData<in Any> = liveInitWatcher
    fun liveErrorEvent(): MutableLiveData<Throwable> = liveErrorWatcher
    fun liveProgressEvent(): MutableLiveData<Boolean> = liveProgressWatcher
}