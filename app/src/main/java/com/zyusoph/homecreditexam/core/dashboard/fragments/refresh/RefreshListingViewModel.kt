package com.zyusoph.homecreditexam.core.dashboard.fragments.refresh

import com.zyusoph.homecreditexam.core.base.BaseViewModel

/**
 * Created by z.y on 11/24/2018.
 */
class RefreshListingViewModel: BaseViewModel() {

    init {
        liveInitWatcher.postValue(null)
    }
}