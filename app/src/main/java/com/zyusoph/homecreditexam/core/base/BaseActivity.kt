package com.zyusoph.homecreditexam.core.base

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by z.y on 11/23/2018.
 */
abstract class BaseActivity: AppCompatActivity() {
    abstract fun observeInit()
}