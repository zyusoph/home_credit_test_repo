package com.zyusoph.homecreditexam.core.detail

import com.zyusoph.homecreditexam.core.base.BaseViewModel

/**
 * Created by z.y on 11/24/2018.
 */
class WeatherDetailViewModel: BaseViewModel() {

    companion object {
        const val KEY_WEATHER_MODEL_GSON_INTENT = "weather_model_gson"
    }

    init {
        liveInitWatcher.postValue(null)
    }
}