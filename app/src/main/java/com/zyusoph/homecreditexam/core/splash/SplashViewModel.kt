package com.zyusoph.homecreditexam.core.splash

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.zyusoph.homecreditexam.core.base.BaseViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by z.y on 11/23/2018.
 */
class SplashViewModel: BaseViewModel() {

    init {

        Observable.timer(2000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    liveInitWatcher.postValue(null)
                }
    }
}