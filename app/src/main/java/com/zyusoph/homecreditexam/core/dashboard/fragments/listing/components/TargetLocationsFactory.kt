package com.zyusoph.homecreditexam.core.dashboard.fragments.listing.components

import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.WeatherListingViewModel

/**
 * Created by z.y on 11/24/2018.
 */
val targetLocations by lazy {

    listOf(
            WeatherListingViewModel.TargetLocation(51.5074, 0.1278, "London"),
            WeatherListingViewModel.TargetLocation(50.0755, 14.4378, "Prague"),
            WeatherListingViewModel.TargetLocation(37.7749, 122.4194, "San Fransisco")
    )
}