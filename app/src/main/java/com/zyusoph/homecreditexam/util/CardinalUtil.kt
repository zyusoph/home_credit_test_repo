package com.zyusoph.homecreditexam.util

/**
 * Created by z.y on 11/24/2018.
 */
fun fromDegreeToCardinal(deg: Float): String {

    if (deg == 0.0f) {
        return ""
    }

    val directions = arrayOf("N", "NE", "E", "SE", "S", "SW", "W", "NW")
    return directions[(Math.round((((deg.toDouble() % 360)) / 45)) % 8).toInt()]
}