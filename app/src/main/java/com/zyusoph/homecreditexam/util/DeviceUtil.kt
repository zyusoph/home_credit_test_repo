package com.zyusoph.homecreditexam.util

import android.content.Context
import android.location.LocationManager
import java.lang.ref.WeakReference
import android.net.NetworkInfo
import android.net.ConnectivityManager

/**
 * Created by z.y on 7/18/18.
 */

// this maybe an overhead when using WeakReferences, but I'd rather lean on little redundancies
// than risking leaking things on the long run
fun isGpsEnabled(weakContext: WeakReference<Context>): Boolean {
    return (weakContext.get()!!.getSystemService(Context.LOCATION_SERVICE ) as LocationManager).isProviderEnabled(LocationManager.GPS_PROVIDER)
}

fun hasInternetConnectivity(context: WeakReference<Context>): Boolean {

    val cm = context.get()!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
    return activeNetwork?.isConnected == true
}