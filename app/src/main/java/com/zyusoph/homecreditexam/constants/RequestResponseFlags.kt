package com.zyusoph.homecreditexam.constants

/**
 * Created by z.y on 11/24/2018.
 */
class RequestResponseFlags {

    companion object {
        const val PERMISSION_REQUEST = 69
        const val LOCATION_SETTINGS_REQUEST = 100
    }
}