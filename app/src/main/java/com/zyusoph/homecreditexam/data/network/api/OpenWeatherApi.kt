package com.zyusoph.homecreditexam.data.network.api

import com.zyusoph.homecreditexam.data.network.response.OpenWeatherCurrentConditionResponse
import com.zyusoph.homecreditexam.data.network.response.OpenWeatherForecastResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by z.y on 3/24/2018.
 */
interface OpenWeatherApi {

    companion object {
        const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
        const val API_KEY = "6ce4a3446d1646d5e9971045d118f35c"
    }

    class ErrorCode {

        companion object {
            const val CODE_429 = 429 // Unauthorized, too many requests
            const val CODE_401 = 401 // Invalid key
        }
    }

    @GET("weather?")
    fun getCurrentCondition(
            @Query("APPID") appId: String,
            @Query("lat") latitudeStr: String,
            @Query("lon") longitudeStr: String,
            @Query("units") unit: String
    ) : Observable<OpenWeatherCurrentConditionResponse>

    @GET("forecast?")
    fun getForecast(
            @Query("APPID") appId: String,
            @Query("lat") latitudeStr: String,
            @Query("lon") longitudeStr: String,
            @Query("units") unit: String
    ): Observable<OpenWeatherForecastResponse>
}