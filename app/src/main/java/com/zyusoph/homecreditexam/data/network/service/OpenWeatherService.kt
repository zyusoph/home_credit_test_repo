package com.zyusoph.homecreditexam.data.network.service

import com.zyusoph.homecreditexam.data.network.api.OpenWeatherApi
import com.zyusoph.homecreditexam.data.network.response.OpenWeatherCurrentConditionResponse
import io.reactivex.Observable

/**
 * Created by z.y on 11/23/2018.
 */
class OpenWeatherService {

    fun currentCondition(latitude: Double, longitude: Double): Observable<OpenWeatherCurrentConditionResponse> {

        val openWeatherApi = ClientBuilder.getBuilderInstance()!!.openWeatherApi()

        return openWeatherApi.getCurrentCondition(
                OpenWeatherApi.API_KEY,
                "$latitude",
                "$longitude",
                "metric"
        )
    }
}