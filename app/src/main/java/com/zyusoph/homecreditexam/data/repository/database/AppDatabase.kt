package com.zyusoph.homecreditexam.data.repository.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.zyusoph.homecreditexam.data.repository.dao.LastWeatherDao
import com.zyusoph.homecreditexam.data.repository.entity.LastWeatherEntity

/**
 * Created by z.y on 11/26/2018.
 */
@Database(
        entities = arrayOf(
                LastWeatherEntity::class
        ),
        version = 1,
        exportSchema = false
)
abstract class AppDatabase: RoomDatabase() {

    abstract fun lastWeatherDao(): LastWeatherDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(context: Context): AppDatabase? {

            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context, AppDatabase::class.java, "home_credit_exam").build()
            }
            return INSTANCE
        }

        fun destroyInstance() {

            INSTANCE = null
        }
    }
}