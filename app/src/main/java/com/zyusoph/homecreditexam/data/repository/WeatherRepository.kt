package com.zyusoph.homecreditexam.data.repository

import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.WeatherListingViewModel
import com.zyusoph.homecreditexam.core.dashboard.fragments.listing.components.targetLocations
import com.zyusoph.homecreditexam.core.model.WeatherModel
import com.zyusoph.homecreditexam.core.model.mapper.WeatherModelMapper
import com.zyusoph.homecreditexam.data.network.response.OpenWeatherCurrentConditionResponse
import com.zyusoph.homecreditexam.data.network.service.OpenWeatherService
import com.zyusoph.homecreditexam.data.repository.database.AppDatabase
import com.zyusoph.homecreditexam.data.repository.entity.WeatherEntityMapper
import com.zyusoph.homecreditexam.exception.NoWeatherDataException
import io.reactivex.*
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

/**
 * Created by z.y on 11/23/2018.
 */
class WeatherRepository(weatherService: OpenWeatherService, appDatabase: AppDatabase) {

    private val weatherService = weatherService
    private val appDatabase = appDatabase

    fun fetchCurrentCondition(targetLocations: List<WeatherListingViewModel.TargetLocation>): Single<List<WeatherModel>> {

        return Observable.zip(Observable.fromIterable(targetLocations)
                , Observable.interval(300, TimeUnit.MILLISECONDS),
                BiFunction<WeatherListingViewModel.TargetLocation, Long, WeatherListingViewModel.TargetLocation> {
                    p1: WeatherListingViewModel.TargetLocation, _: Long ->
                    p1
                })
                .flatMap {
                    targetLocation: WeatherListingViewModel.TargetLocation ->
                    weatherService
                            .currentCondition(targetLocation.latitude, targetLocation.longitude)
                            .flatMap {

                                val weatherModel = WeatherModelMapper.transform(it, targetLocation.place)
                                Observable.just(weatherModel)
                            }
                }
                .toList()
                .doAfterSuccess({

                    appDatabase.lastWeatherDao().deleteLastWeather()

                    val weatherEntities = WeatherEntityMapper.transform(it)
                    weatherEntities.forEach {
                        appDatabase.lastWeatherDao().insertCurrentWeather(it)
                    }
                })
                .onErrorResumeNext {

                    val weatherModels = WeatherModelMapper.transform(appDatabase.lastWeatherDao().getAllWeather())

                    if (weatherModels.isEmpty()) {
                        throw NoWeatherDataException("No data available for last weather.")
                    }

                    Single.just(weatherModels)
                }
    }
}