package com.zyusoph.homecreditexam.data.repository.entity

import com.zyusoph.homecreditexam.core.model.WeatherModel
import com.zyusoph.homecreditexam.data.network.response.OpenWeatherCurrentConditionResponse
import com.zyusoph.homecreditexam.util.fromDegreeToCardinal

/**
 * Created by z.y on 11/26/2018.
 */
class WeatherEntityMapper {

    companion object {

        fun transform(response: OpenWeatherCurrentConditionResponse, locationName: String): LastWeatherEntity {

            var lastWeatherEntity = LastWeatherEntity()

            lastWeatherEntity.place = locationName
            lastWeatherEntity.temperature = "${response.main.temperature}"
            lastWeatherEntity.iconSrc = response.weather[0].icon
            lastWeatherEntity.condition = response.weather[0].main
            lastWeatherEntity.description = response.weather[0].description
            lastWeatherEntity.pressure = "${response.main.pressure}"
            lastWeatherEntity.humidity = "${response.main.humidity}"
            lastWeatherEntity.minTemp = "${response.main.temperatureMin}°C"
            lastWeatherEntity.maxTemp = "${response.main.temperatureMax}°C"
            lastWeatherEntity.visibility = "${response.main.temperatureMax}°C"
            lastWeatherEntity.windSpeed = "${response.wind.speed} kph"
            lastWeatherEntity.windDegree = fromDegreeToCardinal(response.wind.deg)

            return lastWeatherEntity
        }

        fun transform(weatherModels: List<WeatherModel>): List<LastWeatherEntity> {

            val lastWeatherEntities = ArrayList<LastWeatherEntity>()

            weatherModels.forEach {

                var lastWeatherEntity = LastWeatherEntity()

                lastWeatherEntity.place = it.place
                lastWeatherEntity.temperature = it.temperature
                lastWeatherEntity.iconSrc = it.iconSrc
                lastWeatherEntity.condition = it.condition
                lastWeatherEntity.description = it.description
                lastWeatherEntity.pressure = it.pressure
                lastWeatherEntity.humidity = it.humidity
                lastWeatherEntity.minTemp = it.minTemp
                lastWeatherEntity.maxTemp = it.maxTemp
                lastWeatherEntity.visibility = it.visibility
                lastWeatherEntity.windSpeed = it.windSpeed
                lastWeatherEntity.windDegree = it.windDegree

                lastWeatherEntities.add(lastWeatherEntity)
            }

            return lastWeatherEntities
        }
    }
}