package com.zyusoph.homecreditexam.data.repository.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by z.y on 11/26/2018.
 */
@Entity(tableName = "tbl_last_weather")
class LastWeatherEntity {

    @PrimaryKey(autoGenerate = true)
    var lastWeatherEntityId: Int = 0

    @ColumnInfo(name="place")
    var place: String = ""

    @ColumnInfo(name="temperature")
    var temperature: String = ""

    @ColumnInfo(name="iconSrc")
    var iconSrc: String = ""

    @ColumnInfo(name="condition")
    var condition: String = ""

    @ColumnInfo(name="description")
    var description: String = ""

    @ColumnInfo(name="pressure")
    var pressure: String = ""

    @ColumnInfo(name="humidity")
    var humidity: String = ""

    @ColumnInfo(name="minTemp")
    var minTemp: String = ""

    @ColumnInfo(name="maxTemp")
    var maxTemp: String = ""

    @ColumnInfo(name="visibility")
    var visibility: String = ""

    @ColumnInfo(name="windSpeed")
    var windSpeed: String = ""

    @ColumnInfo(name="windDegree")
    var windDegree: String = ""
}