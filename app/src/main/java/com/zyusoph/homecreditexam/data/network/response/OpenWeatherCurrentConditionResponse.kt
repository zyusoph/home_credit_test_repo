package com.zyusoph.homecreditexam.data.network.response

import com.google.gson.annotations.SerializedName

/**
 * Created by z.y on 3/24/2018.
 */
data class OpenWeatherCurrentConditionResponse(

        @SerializedName("weather") val weather: List<CurrentWeather>,
        @SerializedName("base") val base: String,
        @SerializedName("main") val main: CurrentMain,
        @SerializedName("wind") val wind: CurrentWind,
        @SerializedName("sys") val system: CurrentSystem,
        @SerializedName("clouds") val clouds: CurrentClouds,
        @SerializedName("visibility") val visibility: String
)

data class CurrentWeather(

        @SerializedName("main") val main:String,
        @SerializedName("description") val description: String,
        @SerializedName("icon") val icon: String,
        @SerializedName("clouds") val clouds: CurrentClouds
)

data class CurrentMain(

        @SerializedName("temp") val temperature: Float,
        @SerializedName("pressure") val pressure: Float,
        @SerializedName("humidity") val humidity: Int,
        @SerializedName("temp_min") val temperatureMin: Float,
        @SerializedName("temp_max") val temperatureMax: Float,
        @SerializedName("sea_level") val seaLevel: Float,
        @SerializedName("ground_level") val groundLevel: Float
)

data class CurrentWind(

        @SerializedName("speed") val speed: Float,
        @SerializedName("deg") val deg: Float
)

data class CurrentClouds(
        @SerializedName("all") val all: Int
)

data class CurrentSystem(
        @SerializedName("message") val message: Float,
        @SerializedName("country") val country: String,
        @SerializedName("sunrise") val sunrise: Long,
        @SerializedName("sunset") val sunset: Long
)
