import com.zyusoph.homecreditexam.data.network.api.OpenWeatherApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Created by z.y on 3/24/2018.
 */
class ClientBuilder private constructor() {

    companion object {

        private var instance: ClientBuilder? = null

        fun getBuilderInstance(): ClientBuilder? {
            synchronized(this) {
                if (instance == null) {
                    synchronized(this) {
                        if(instance == null) {
                            instance = ClientBuilder()
                        }
                    }
                }
            }

            return instance
        }
    }

    fun openWeatherApi(): OpenWeatherApi {

        return getClientBuilder()
                .baseUrl(OpenWeatherApi.BASE_URL)
                .build()
                .create(OpenWeatherApi::class.java)
    }

    private fun getClientWithLogging(): OkHttpClient {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()
    }

    private fun getClientBuilder(): Retrofit.Builder {

        return Retrofit.Builder()
                .client(getClientWithLogging())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }
}