package com.zyusoph.homecreditexam.data.network.response

import com.google.gson.annotations.SerializedName

/**
 * Created by z.y on 3/31/2018.
 */
data class OpenWeatherForecastResponse(
        @SerializedName("list") val forecastResponseList: List<ForecastResponse>
)

data class ForecastResponse (
        @SerializedName("dt") val timeMillisForecast: Long,
        @SerializedName("main") val main: ForecastResponseMain,
        @SerializedName("weather") val weather: List<ForecastResponseWeather>,
        @SerializedName("wind") val wind: ForecastResponseWind
)

data class ForecastResponseMain(

        @SerializedName("temp") val temperature: Float,
        @SerializedName("temp_min") val temperatureMin: Float,
        @SerializedName("temp_max") val temperatureMax: Float,
        @SerializedName("pressure") val pressure: Float,
        @SerializedName("humidity") val humidity: Int
)

data class ForecastResponseWeather(

        @SerializedName("id") val id: Int,
        @SerializedName("main") val main:String,
        @SerializedName("description") val description: String,
        @SerializedName("icon") val icon: String
)

data class ForecastResponseWind(

        @SerializedName("speed") val speed: Float,
        @SerializedName("deg") val deg: Float
)