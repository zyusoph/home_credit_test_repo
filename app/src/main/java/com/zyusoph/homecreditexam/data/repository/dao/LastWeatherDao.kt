package com.zyusoph.homecreditexam.data.repository.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.zyusoph.homecreditexam.data.repository.entity.LastWeatherEntity

/**
 * Created by z.y on 11/26/2018.
 */
@Dao
abstract class LastWeatherDao {

    @Query("SELECT * FROM tbl_last_weather")
    abstract fun getAllWeather(): List<LastWeatherEntity>

    @Insert
    abstract fun insertCurrentWeather(lastWeatherEntity: LastWeatherEntity)


    @Query("DELETE FROM tbl_last_weather")
    abstract fun deleteLastWeather()
}