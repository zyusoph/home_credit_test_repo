package com.zyusoph.homecreditexam.exception

/**
 * Created by z.y on 11/26/2018.
 */
class NoWeatherDataException(msg: String) : RuntimeException(msg)