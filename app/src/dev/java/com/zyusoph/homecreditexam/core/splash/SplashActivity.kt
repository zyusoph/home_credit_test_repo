package com.zyusoph.homecreditexam.core.splash

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.zyusoph.homecreditexam.R
import com.zyusoph.homecreditexam.core.base.BaseActivity
import com.zyusoph.homecreditexam.core.dashboard.DashboardActivity

class SplashActivity : BaseActivity() {

    private lateinit var splashViewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        splashViewModel = SplashViewModel()
        observeInit()
    }

    override fun observeInit() {

        splashViewModel.liveInitEvent().observe(this, Observer {

            var intent = DashboardActivity.newInstance(this)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        })
    }
}